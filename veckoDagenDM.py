'''
Stockholms University DSV
Program: Datavetenskap
Course: Introduktion till Data och Systemvetenskap
Term: HT2019
Student: Dimitrios Mavromatis


Expections to be raised. Inset integer numbers only
Exception year between 1583 to 9999
Exception month number between 1-12
Exception day between 1-30
Excpetion day between 1-29
'''



def isthirty_month(month):
    return month == 4 or month == 6 or month == 9 or month == 11

def isthirty_one_month(month):
    return month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12

def isleap_february(year, month):
    return month == 2 and is_leap(year)

def isnot_leap_february(year, month):
    return month == 2 and not is_leap(year)

def is_leap(year):
    if year % 4 != 0:
        return False
    if year % 100 <= 1 and year % 400 == 0:
        return True
    if not year % 100 == 0 and not year % 400 == 0:
        return True
    return False

def provide_year():
    while True:
        try:
              year = int(input('Year: '))
              if 1583 <= year <= 9999:
                  return year
              else:
                    print('Out of allowed range 1583 to 9999 ')
        except ValueError:
              print('\nInsert integer number only!')


def provide_month():
      while True:
            try:
                  month = int(input('Month: '))
                  if 1 <= month <= 12:
                        return month
                  else:
                      print('Out of allowed range 1 to 12')
            except ValueError:
                  print('\nInsert integer number only!')


def provide_day(month, year):
      while True:
            try:
                day = int(input('Day: '))
                if day < 1:
                    print("Out of allowed range 1 to 30")
                elif isthirty_month(month) and day > 30:
                    print("Out of allowed range 1 to 30")
                elif isthirty_one_month(month) and day > 31:
                    print("Out of allowed range 1 to 31")
                elif isleap_february(year, month) and day > 29:
                    print("Out of allowed range 1 to 29")
                elif isnot_leap_february(year, month) and day > 28:
                    print("Out of allowed range 1 to 28")
                else:
                    return day
            except ValueError:
                print('\nInsert integer number only!')


year = provide_year()
month = provide_month()
day = provide_day(month, year)

if month == 1 or month == 2:
    month += 12
    year -= 1

weekday = (day + 13 * (month + 1) // 5 + year + (year // 4) - (year // 100) + (year // 400)) % 7

if weekday == 0:
    print('It is a Saturday')
if weekday == 1:
    print('It is a Sunday')
if weekday == 2:
    print('It is a Monday')
if weekday == 3:
    print('It is a Tuesday')
if weekday == 4:
    print('It is a Wednesday')
if weekday == 5:
    print('It is a Thursday')
if weekday == 6:
    print('It is a Friday')

'''
Räkna ut veckodagen
Instruktioner
Uppgiften ska lösas individuellt.
Resultatet ska vara ett Python-program. Programmets källkod ska lämnas in i inlämningslådan
i iLearn2 (se kursanvisningar för datum för inlämning).
Högst uppe i källkodsfilen i en kommentar ska inlämnarens namn och personnummer stå.
Innan inlämning ska programmet testas i VPL (Virtual Programming Lab) i iLearn2.
Närmare instruktioner om detta kommer senare i iLearn2.
Obs! att VPL-testning innebär att programmets input och output kontrolleras maskinellt. Detta
innebär att utskrifter från programmet måste följa nedanstående instruktioner exakt!
Uppgiften
Uppgiften går ut på att skriva ett program som läser in ett datum på formen år, månad och dag
(tre heltal) och skriver ut vilken veckodag det är.
Veckodagsberäkningen ska göras med en formel som kallas Zellers kongruens, se nedan.
Programdialogen ska se ut så här (användarens inmatning i svart text):
Year: 2018
Month: 8
Day: 21
It is a Tuesday
Användarens inmatning ska kontrolleras och varje fråga ska upprepas tills användaren har
besvarat den korrekt. Programdialogen skulle alltså kunna se ut så här:
Year: 1066
Out of allowed range 1583 to 9999
Year: 20178
Out of allowed range 1583 to 9999
Year: 2018
Month: 15
Out of allowed range 1 to 12
Month: 9
Day: 0
Out of allowed range 1 to 30
Day: 31
Out of allowed range 1 to 30
Day: 13
It is a Thursday 
Följande ska kontrolleras:
 att årtalet är i intervallet 1583 – 9999
 att månaden är i intervallet 1-12
 att dagnummer inom månaden stämmer överens med månadsnumret, dvs att om
månaden är 1, 3, 5, 7, 8, 10 eller 12 så är dagnummer i intervallet 1-31, om månaden
är 4, 6, 9, eller 11 så är dagnummer i intervallet 1-30 och om månaden är 2 så är
dagnummer i intervallet 1-28 eller 1-29 beroende på om det är skottår eller ej.
Det är skottår om årtalet är jämnt delbart med 400, eller om det är jämnt delbart med 4
men inte jämnt delbart med 100.
Zellers kongruensformel
Zellers kongruens (se https://en.wikipedia.org/wiki/Zeller%27s_congruence) är en formel för
att räkna ut veckodagen. Den börjar gälla år 1583, året efter det att många länder övergick till
den Gregorianska kalendern.

Den börjar med att man kodar om månaden så att januari resp. februari betraktas som
månaderna 13 resp. 14 föregående år:
if month == 1 or month == 2:
 month += 12
 year -= 1
Sedan kan man räkna ut veckodagen ur formeln:
weekday = ( day + 13*(month+1)//5 + year + year//4
 - year//100 + year//400 ) % 7
där veckodagen är kodad som 0 = lördag, 1 = söndag, 2 = måndag, …, 6 = fredag. 
'''